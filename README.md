# gitlab-ci components

[[_TOC_]]

## Complete pipeline components

### gitlab-ci-component (full pipeline)

Provides tests and release pipeline for 🦊[gitlab CI/CD component](https://docs.gitlab.com/ee/ci/components/)
projects.

You can add the full pipeline component to an existing `.gitlab-ci.yml` file by
using the `include:` keyword. (replace `<VERSION>` with the latest released tag or `main`)

```yaml
include:
  - component: gitlab.com/jeremy.petit/gitlab-ci/gitlab-ci-component@<VERSION>
    # Uncomment following lines to override default values
    # inputs:
    #   test-stage:           'test'
    #   release-stage:        'release'
    #   gitleaks-image-name:  'zricethezav/gitleaks'
    #   gitleaks-image-tag:   'latest'
    #   gitleaks-report-path: 'gl-secret-detection-report.gitleaks.json'
    #   yamllint-image-name:  'registry.gitlab.com/pipeline-components/yamllint'
    #   yamllint-image-tag:   'latest'
    #   yamllint-report:      true
    #   yamllint-report-path: 'gl-code-quality-report.yamllint.json'
    #   go-semrel-image-name: 'registry.gitlab.com/go-semantic-release/semantic-release'
    #   go-semrel-image-tag:  'latest'
    #   go-semrel-tag-match:  '^[0-9]+\.[0-9]+\.[0-9]+$'
    #   go-semrel-opts:       '--allow-initial-development-versions --allow-no-changes --changelog-generator-opt "emojis=true"'
```

## Partial pipeline components

### dprint (job)

Provides a [dprint](https://github.com/dprint/dprint)
[![GitHub Repo stars](https://img.shields.io/github/stars/dprint/dprint?style=plastic&label=%E2%AD%90)](https://github.com/dprint/dprint)
format job (check-only).

You can add the partial pipeline component to an existing `.gitlab-ci.yml` file
by using the `include:` keyword. (replace `<VERSION>` with the latest released tag or `main`)

```yaml
include:
  - component: gitlab.com/jeremy.petit/gitlab-ci/dprint@<VERSION>
    # Uncomment following lines to override default values
    # inputs:
    #   stage:       'test'
```

### gitleaks (job)

Provides a [gitleaks](https://github.com/gitleaks/gitleaks)
[![GitHub Repo stars](https://img.shields.io/github/stars/gitleaks/gitleaks?style=plastic&label=%E2%AD%90)](https://github.com/gitleaks/gitleaks)
linter job.

You can add the partial pipeline component to an existing `.gitlab-ci.yml` file
by using the `include:` keyword. (replace `<VERSION>` with the latest released tag or `main`)

```yaml
include:
  - component: gitlab.com/jeremy.petit/gitlab-ci/gitleaks@<VERSION>
    # Uncomment following lines to override default values
    # inputs:
    #   stage:       'test'
    #   image-name:  'zricethezav/gitleaks'
    #   image-tag:   'latest'
    #   report-path: 'gl-secret-detection-report.gitleaks.json'
```

### go-semantic-release (job)

Provides a [go-semantic-release](https://github.com/go-semantic-release/semantic-release)
[![GitHub Repo stars](https://img.shields.io/github/stars/go-semantic-release/semantic-release?style=plastic&label=%E2%AD%90)](https://github.com/go-semantic-release/semantic-release)
linter job.

You can add the partial pipeline component to an existing `.gitlab-ci.yml` file by using the `include:` keyword. (replace `<VERSION>` with the latest released tag or `main`)

```yaml
include:
  - component: gitlab.com/jeremy.petit/gitlab-ci/go-semantic-release@<VERSION>
    # Uncomment following lines to override default values
    # inputs:
    #   stage:               '.post'
    #   image-name:          'registry.gitlab.com/go-semantic-release/semantic-release'
    #   image-tag:           'latest'
    #   go-semrel-tag-match: '^[0-9]+\.[0-9]+\.[0-9]+$'
    #   go-semrel-opts:      '--allow-initial-development-versions --allow-no-changes --changelog-generator-opt "emojis=true"'
```

To create the release, this job rely on a gitlab token with `api` access in `SEMREL_GITLAB_TOKEN` CI-variable.

:warning: This job will only run on the default branch!

### yamllint (job)

Provides a [yamllint](https://github.com/adrienverge/yamllint)
[![GitHub Repo stars](https://img.shields.io/github/stars/adrienverge/yamllint?style=plastic&label=%E2%AD%90)](https://github.com/adrienverge/yamllint)
linter job.

You can add the partial pipeline component to an existing `.gitlab-ci.yml` file
by using the `include:` keyword. (replace `<VERSION>` with the latest released tag or `main`)

```yaml
include:
  - component: gitlab.com/jeremy.petit/gitlab-ci/yamllint@<VERSION>
    # Uncomment following lines to override default values
    # inputs:
    #   stage:       'test'
    #   image-name:  'registry.gitlab.com/pipeline-components/yamllint'
    #   image-tag:   'latest'
    #   report:      true
    #   report-path: 'gl-code-quality-report.yamllint.json'
```
